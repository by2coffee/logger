/*
 * log.h
 *
 *  Created on: 14-Jul-2016
 *      Author: manish
 */

#ifndef SRC_LOGGER_LOG_H_
#define SRC_LOGGER_LOG_H_

void LOG_init(const char * name);
void LOG_log(const char * level, const char * format, ...);

#define LOG_debug(format, ...) LOG_log("DEBUG", format, ##__VA_ARGS__)
#define LOG_info(format, ...) LOG_log("INFO", format, ##__VA_ARGS__)
#define LOG_warning(format, ...) LOG_log("WARNING", format, ##__VA_ARGS__)
#define LOG_error(format, ...) LOG_log("ERROR", format, ##__VA_ARGS__)
#define LOG_critical(format, ...) LOG_log("CRITICAL", format, ##__VA_ARGS__)

#endif /* SRC_LOGGER_LOG_H_ */
