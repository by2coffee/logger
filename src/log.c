/*
 * log.c
 *
 *  Created on: 14-Jul-2016
 *      Author: manish
 */

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define TIME_FORMAT "%Y-%m-%d %H:%M:%S %Z"
#define TIME_DEFAULT "\?\?\?\?-\?\?-\?\? \?\?:\?\?:\?\? \?\?\?"
#define TIME_MAX_LENGTH 24
#define STRLEN(s) (sizeof(s)/sizeof(s[0]))

static const char * process_name;
static int pid;

void LOG_init(const char * name) {
	if (NULL == name) {
		process_name = "?";
	}
	else {
		process_name = name;
	}
	pid = getpid();
}

void get_timestamp(char * buffer, int buffer_length) {
	time_t epoch = time(NULL);
	if (epoch == -1) {
		strncpy(buffer, TIME_DEFAULT, buffer_length);
		buffer[buffer_length - 1] = '\0';
		return;
	}

	struct tm * current_time = localtime(&epoch);
	if (!strftime(buffer, buffer_length, TIME_FORMAT, current_time)) {
		strncpy(buffer, TIME_DEFAULT, buffer_length);
		buffer[buffer_length - 1] = '\0';
		return;
	}
	return;
}

void LOG_log(const char * level, const char * format, ...) {
	if (NULL == format || NULL == level) {
		return;
	}
	va_list args;
	va_start(args, format);
	char timestamp[TIME_MAX_LENGTH];
	get_timestamp(timestamp, TIME_MAX_LENGTH);
	fprintf(stderr, "%s %s[%d] %s ", timestamp, process_name, pid, level);
	vfprintf(stderr, format, args);
	fprintf(stderr, "\n");
	va_end(args);
}

