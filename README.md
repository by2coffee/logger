Simple C Logger
=========
Single source and header file console logger implementation in C, **without** whistles and bells

## Why
Because I needed a quick and dirty logging routines when I shifted from prototyping in Python to prototyping in C. 
I am sure there are better and production-ready loggers (syslog, zlog) for C, but I needed something that was simple to setup.

## Usage
`main.c`
```C
#include "log.h"

int main(int argc, char **argv) {
    LOG_init("loggerdemo"); //Used as the program name in log
    ...
    
    LOG_info("Simple string")
    LOG_info("Formatted %s", "string")
    //Other log levels supported are LOG_debug(), LOG_warning(), LOG_error(), LOG_critical()
    ...
}
```
Console (stderr) output:
```
2016-08-29 17:20:00 IST loggerdemo[1712] INFO Simple string
2016-08-29 17:21:55 IST loggerdemo[1712] INFO Formatted string
```

## Build
Just include the log.c as a source file

`gcc main.c log.c -o loggerdemo`

## Requirements
Should work on all Linux based systems. Worked on OpenWRT and Debian Jessie (for Raspberry Pi)

## Running the Tests
No tests yet. Not tested extensively 

## License
MIT License. See the bundled LICENSE file for details.